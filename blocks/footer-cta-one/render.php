<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['local_path'] = get_stylesheet_directory_uri() . "/blocks/footer-cta-one";

Timber::render( 'template.twig', $context);