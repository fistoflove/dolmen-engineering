<?php
if ( ! function_exists('Case_Study_post_type') ) {

// Register Custom Post Type
function Case_Study_post_type() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Case Studies', 'ims_theme' ),
		'name_admin_bar'        => __( 'Case Study', 'ims_theme' ),
		'archives'              => __( 'Case Study Archives', 'ims_theme' ),
		'attributes'            => __( 'Case Study Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Case Studies', 'ims_theme' ),
		'add_new_item'          => __( 'Add Case Study', 'ims_theme' ),
		'add_new'               => __( 'Add Case Study', 'ims_theme' ),
		'new_item'              => __( 'New Case Study', 'ims_theme' ),
		'edit_item'             => __( 'Edit Case Study', 'ims_theme' ),
		'update_item'           => __( 'Update Case Study', 'ims_theme' ),
		'view_item'             => __( 'View Case Study', 'ims_theme' ),
		'view_items'            => __( 'View Case Studies', 'ims_theme' ),
		'search_items'          => __( 'Search Case Study', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Case Study', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'ims_theme' ),
		'items_list'            => __( 'Case studies list', 'ims_theme' ),
		'items_list_navigation' => __( 'Case studies list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter case studies list', 'ims_theme' ),
	);
	$rewrite = array(
		'slug'                  => 'our-work',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Case Study', 'ims_theme' ),
		'description'           => __( 'Case success studies', 'ims_theme' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'excerpt' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => false,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'Case_Study', $args );

}
add_action( 'init', 'Case_Study_post_type', 0 );

}