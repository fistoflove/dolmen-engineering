<?php

// Register custom post types
require_once('register-custom-post-types.php');

function acf_filter_rest_api_preload_paths( $preload_paths ) {
    if ( ! get_the_ID() ) {
      return $preload_paths;
    }
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
    $v1 =  array_filter(
      $preload_paths,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
    return array_filter(
      $v1,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
  }
  add_filter( 'block_editor_rest_api_preload_paths', 'acf_filter_rest_api_preload_paths', 10, 1 );


add_action( 'wp_enqueue_scripts', function(){
  $post = get_post();
  if(!strpos($post->post_content, "wp:acf/contact") or !$post->ID == 200) {
    wp_dequeue_script( 'contact-form-7' );
    wp_dequeue_style( 'contact-form-7' );
    wp_dequeue_script( 'google-recaptcha' );
    wp_dequeue_script( 'wpcf7-recaptcha' );
  }
}, 99 );

?>

