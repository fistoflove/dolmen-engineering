module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      dev: {
        files: {
          "static/main.css": "dev/scss/source.scss",
          "static/cs.css": "dev/scss/cs.scss",
          "static/cs-index.css": "dev/scss/cs-index.scss",
        },
      },
    },
    csso: {
      compress: {
        options: {
          report: "gzip",
        },
        files: {
          "static/main.min.css": ["static/main.css"]
        },
      },
    },
    watch: {
      files: ["dev/scss/*", "dev/scss/theme/*"],
      tasks: ["sass", "csso"],
    },
  });
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-csso");
  grunt.loadNpmTasks("grunt-contrib-watch");
};
